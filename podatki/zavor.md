# Zavorna pot

Podatki: [zavor.csv](zavor.csv)

## Opis

Meritve hitrosti in zavorne poti na vzorcu 50 avtomobilov.

## Format

Baza podatkov s 50 meritvami dveh spremenljivk

* *hitrost* je numerična zvezna spremenljivka, ki predstavlja hitrost avtomobila (v kilometrih
na uro).
* *pot* je numerična zvezna spremenljivka, ki predstavlja zavorno pot (v metrih).

## Raziskovalna domneva

Med zavorno potjo in hitrostjo avtomobila obstaja funkcijska zveza.

## Opomba

Konstruirati linearni regresijski model med spremenljivkama `sqrt(pot)` in `hitrost`.