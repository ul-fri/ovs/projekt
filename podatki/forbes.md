# Zračni tlak
Podatki: [forbes.csv](forbes.csv)

## Opis

Meritve zračnega tlaka in temperature vretja vode na 17 lokacijah v Alpah, ki jih je zbral
škotski zik James D. Forbes.

## Format

Baza podatkov s 17 meritvami dveh spremenljivk

* *temp* je numerična zvezna spremenljivka, ki predstavlja temperaturo vretja vode (v stopin-
jah Celzija).
* *tlak* je numerična zvezna spremenljivka, ki predstavlja zračni tlak (v milibarih).

## Raziskovalna domneva

Med zračnim tlakom in temperaturo vretja vode obstaja funkcijska zveza.

## Opomba

Konstruirati linearni regresijski model med spremenljivkama `log10(tlak)` in `temp`.