# Poraba goriva

Podatki: [avto.csv](avto.csv)

## Opis

Meritve mase in porabe goriva na vzorcu 32 avtomobilov.

## Format

Baza podatkov z 32 vrednostmi treh spremenljivk

* *model* je nominalna spremenljivka, katere vrednosti so nazivi modela avtomobila.
* *masa* je numerična zvezna spremenljivka, ki predstavlja maso avtomobila (v kilogramih).
* *kml* je numerična zvezna spremenljivka, ki predstavlja porabo goriva avtomobila (v
prevoženih kilometrih na liter goriva).

## Raziskovalna domneva

Med porabo goriva in maso avtomobila obstaja linearna funkcijska zveza.