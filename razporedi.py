#!/usr/bin/python3
# 
# Razporedi študentske projekte pri OVS po podatkih, ki so jih
# obdelovali.
#  
import os
import sys
import re
import shutil

RE_FRI = re.compile(r'=63')
RE_UI = re.compile(r'=04')
RANGE_TPL = "{:02d}-"

PROJEKTI = [
('martin/svinec'    , RE_FRI, range(00, 10)),
('martin/geom'      , RE_FRI, range(22, 31)),
('kristina/premor'  , RE_FRI, range(43, 51)),
('kristina/industr' , RE_FRI, range(62, 71)),
('alex/urpostav'    , RE_UI , range(19, 99)),
('alex/avgume'      , RE_FRI, range(80, 85)),
('alex/jezero'      , RE_FRI, range(11, 21)),
('alex/avcena'      , RE_UI , range( 1, 18)),
('kristina/mozgani' , RE_FRI, range(32, 42)),
('martin/klek'      , RE_FRI, range(52, 61)),
('martin/zavor'     , RE_FRI, range(72, 79)),
('kristina/forbes'  , RE_FRI, range(86, 99)),
]


if __name__ == "__main__":
    dirs = filter(lambda x: os.path.isdir(x), os.listdir())
    for dir in dirs:
        print(dir)
        for projekt in PROJEKTI:
            ime, faks, razpon = projekt
            print(ime, faks, razpon)
            os.makedirs(ime, exist_ok=True)
            if faks.search(dir):
                for n in razpon:
                    if dir.find(RANGE_TPL.format(n)) > 0:
                        print("Moving {} to {}".format(dir, ime))
                        shutil.move(dir, ime)
                        break
