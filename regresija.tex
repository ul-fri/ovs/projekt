\documentclass[a4paper,12pt,openany]{book}
\usepackage{fancyhdr}
\usepackage{hyperref}
\usepackage[utf8x]{inputenc}
% \usepackage[slovene]{babel}
\usepackage{url}
\usepackage{enumerate}
\usepackage{graphicx,epsfig,amsmath,amssymb}
\usepackage[svgnames]{xcolor}
\usepackage[gen]{eurosym}
\usepackage{tikz}
\usepackage{verbatim} %za \begin{comment} ... \end{comment}
\usepackage{tikz-qtree}
\setlength\parindent{0pt}
\usepackage{tcolorbox}
\usepackage{multirow}
\usepackage{enumitem}
\usepackage{emptypage}
\renewcommand{\figurename}{Slika}


\usepackage{setspace}



\setcounter{secnumdepth}{0}


\renewcommand{\chaptername}{Poglavje}
\renewcommand\contentsname{Kazalo}

\renewcommand{\:}{\ensuremath{.}}
\setlength{\evensidemargin}{-5mm}
\setlength{\oddsidemargin}{-5mm}
\setlength{\headheight}{20mm}
\setlength{\headsep}{10mm}
\setlength{\textheight}{253mm}
\setlength{\textwidth}{170mm}
\setlength{\footskip}{8mm}
\setlength{\topmargin}{-3.5cm}
\setlength{\parindent}{0pt}

\date{}
\begin{document}
\title{\color{red} Primer statistične obdelave podatkov: linearna regresija}
\author{Kristina Veljković}
\maketitle
\pagestyle{fancy}
\fancyhf{} 
\fancyfoot[C]{\thepage}
\fancyhead[C]{\leftmark}
\renewcommand{\chaptermark}[1]{\markboth{#1}{}}

	
\tableofcontents{}
\chapter{Linearna regresija}
Regresijska analiza je metoda raziskovanja funkcijske zveze med 
spremenljivkami. Zanimala nas bo linearna zveza 
med dvema spremenljivkama (\emph{enostavna linearna regresija})

\[
Y=a+b X +e,
\]

kjer je $e$ naključna napaka. Slučajna spremenljivka $X$ se imenuje \emph{neodvisna spremenljivka}
ali \emph{prediktor}, slučajna spremenljivka $Y$ pa \emph{odvisna spremenljivka}. Vrednosti spremenljivke $X$ lahko raziskovalec izbere, 
oziroma vnaprej fiksira ali kontrolira. 

\section{Koeficient korelacije}
Moč povezanosti med slučajnima zveznima spremenljivkama $Y$ in $X$ merimo s pomočjo koeficienta korelacije $\rho(X,Y)$, ki je enak
\[
\rho(X,Y)=\frac{Cov(X,Y)}{\sigma_X\sigma_Y}=\frac{E\left((X-E(X))(Y-E(Y))\right)}{\sigma_X\sigma_Y},
\]
kjer je $Cov(X,Y)$ kovarianca slučajnih spremenljivk $X$ in $Y$, $\sigma_X$ in $\sigma_Y$ pa standardna odklona $X$ in $Y$. Velja
\[
-1\leq\rho(X,Y)\leq 1,
\]
kjer je $\rho(X,Y)=\pm 1$ v primeru linearne odvisnosti med $X$ in $Y$, oziroma $Y=a+bX$. 
Znak povezanosti kaže na smer povezanosti spremenljivk.

\begin{enumerate}
\item Pozitivna korelacija $\rho(X,Y)>0$ pomeni, da z naraščanjam vrednosti $Y$ naraščajo tudi vrednosti $X$, in 
s padanjem vrednosti $Y$ padajo tudi vrednosti $X$.
\item Negativna korelacija $\rho(X,Y)<0$ pomeni, da z naraščanjam vrednosti $Y$ padajo vrednosti $X$, in 
s padanjem vrednosti $Y$ naraščajo vrednosti $X$.
\end{enumerate}
Absolutna vrednost koeficienta korelacije $|\rho(X,Y)|$ kaže na moč povezanosti med spremenljivkama.
\begin{enumerate}
\item $\rho(X,Y)=0$ ni korelacije med spremenljivkama $X$ in $Y$.
\item $0<|\rho(X,Y)|<0.3$ korelacija med spremenljivkama $X$ in $Y$ je šibka.
\item $0.3<|\rho(X,Y)|<0.7$ korelacija med spremenljivkama $X$ in $Y$ je srednja.
\item $0.7\leq |\rho(X,Y)|\leq 1$ korelacija med spremenljivkama $X$ in $Y$ je močna.
\end{enumerate}

\subsection{Vzorčni koeficient korelacije}
Naj bodo podatki zbrani v parih $(X_1, Y_1), (X_2, Y_2),\ldots, (X_n, Y_n)$,
kjer sta $(X_1, X_2,\ldots X_n)$ in $(Y_1, Y_2,\ldots Y_n)$ vzorca vrednosti
slučajnih spremenljivk $X$ in $Y$. Pearsonov koeficient korelacije je ocena populacijskega koeficienta korelacije med 
spremenljivkama $Y$ in $X$ in je enak
\[
r_{X,Y}=\frac{\sum_{i=1}^n (X_i-\overline{X})(Y_i-\overline{Y})}{(n-1)S_X S_Y},
\]
kjer sta $\overline{X}=\frac{1}{n}\sum_{i=1}^n X_i$ in $\overline{Y}=\frac{1}{n}\sum_{i=1}^n Y_i$ vzorčni povprečji, 
$S_X=\sqrt{\frac{1}{n-1}\sum_{i=1}^n (X_i-\overline{X})^2}$ in
$S_Y=\sqrt{\frac{1}{n-1}\sum_{i=1}^n (Y_i-\overline{Y})^2}$ pa vzorčna standardna odklona slučajnih spremenljivk $X$ in $Y$.

Koeficient determinacije $R^2$ se definira kot delež skupne variabilnosti podatkov, ki pojasnjuje regresijski model.
Predstavlja kvadrat koeficienta korelacije med spremenljivkama $Y$ in $X$. Ima vrednosti med $0$ in $1$; čim bližje je 1, tem boljši
je model (predikcija $Y$ na osnovi $X$ je boljša).



\section{Razsevni diagram}
Prvi korak v ocenjevanju, ali je linearna regresija smiseln model odnosa med dvema spremenljivkama,
je predstaviti dobljene podatke na razsevnem diagramu (ang. \emph{scatter plot}).
Na Sliki 1.1. so prikazani trije grafi podatkov: podatki, ki so linearno povezani,  podatki, ki so nelinearno povezani in podatki, med katerimi
ni povezanosti. V primeru linearne povezanosti se točke na grafu nahajajo v bližini premice, in vzorčni  koeficient korelacije med spremenljivkama
je močen. Na grafu nelinearne zveze med spremenljivkama obstaja funkcijska povezanost, ampak je nelinearna. Pearsonov koficient
korelacije je mera linearne povezanosti med spremenljivkama in ni dobra izbira za ta primer (za monotono nelinearno povezanost se lahko uporabi Spearmanov koeficient
korelacije). Na tretjem grafu je prikazan primer nepovezanih podatkov, na katerem opažamo en ''oblak'' točk.
Koeficient korelacije teh podatkov je blizu 0.
Linearni regresijski model je primeren samo za podatke na prvem grafu.


\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth,height=0.4\textwidth]{slike/zveze.png}
\caption{Grafi linearne, nelinearne in neobstoječe zveze med dvema spremenljivkama}
\end{center}
\end{figure}

Na razsevnem diagramu lahko poskusimo najti točke, ki izstopajo od ostalih točk. Najprej nas zanimajo točke z zelo nizko ali visoko $x$ vrednostjo. Takšne točke se
imenujejo \emph{točke vzvoda} (ang. \emph{leverage points}). Dalje nas zanimajo točke, ki ne sledijo vzorcu (ali trendu) ostalih točk. Takšne točke imajo znatno
višjo ali nižjo $y$ vrednost od ostalih točk in jih imenujemo \emph{osamelci} (ang. \emph{outlier}). Točke, ki so istočasno točke vzvoda in osamelci (imajo nenavadno visoko ali nizko
$x$ in $y$ vrednost), imajo veliki upliv na linearni regresijski model. 


\section{Ocenjevanje parametrov $a$ in $b$}
Naj bodo podatki zbrani v parih $(X_1, Y_1), (X_2, Y_2),\ldots, (X_n, Y_n)$,
kjer sta $(X_1, X_2,\ldots X_n)$ in $(Y_1, Y_2,\ldots Y_n)$ vzorca vrednosti
slučajnih spremenljivk $X$ in $Y$. Če je zveza med spremenljivkama $Y$ in $X$ 
linearna, potem lahko slučajno spremenljivko $Y_i$, $i=1,2,\ldots n$ zapišemo kot
\begin{equation*}
Y_i=E(Y_i)+e_i=a+b X_i +e_i,
\end{equation*}
kjer sta parametra $a$ in $b$ neznana in predstavljata odsek in naklon premice, $e_i$ pa predstavlja naključno napako, ki ni odvisna od $X$. Drugače rečeno, 
vrednosti slučajne spremenljivke $Y_i$ varirajo okoli povprečne vrednosti $E(Y_i)$, ki je linearna funkcija $X_i$. 


Naključne napake $e_i =Y_i-\left(a+bX_i\right)$ predstavljajo vertikalno razdaljo točk $(X_i, Y_i)$ od populacijske regresijske premice (glej Sliko 1.2.).

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\textwidth,height=0.5\textwidth]{slike/napake.png}
\caption{Graf naključnih napak linearnega regresijskega modela}
\end{center}
\end{figure}

\subsection{Metoda najmanjših kvadratov}

Iščemo takšna $a$ in $b$, da minimiziramo vsoto kvadratov naključnih napak,
\begin{equation*}
S(a,b)=\sum_{i=1}^n e_i^2=\sum_{i=1}^n (Y_i-a-bX_i )^2.
\end{equation*}
oziroma minimiziramo skupno vertikalno razdaljo točk od regresijske premice.
Vzemimo parcialna odvoda funkcije $S(a,b)$ po $a$ in po $b$. Dobimo
\begin{align*}
&\frac{\partial S}{\partial a}=-2\sum_{i=1}^n (Y_i-a-bX_i)=0,\\
&\frac{\partial S}{\partial b}=-2\sum_{i=1}^n X_i(Y_i-a-bX_i)=0,
\end{align*}
oziroma
\begin{align*}
&\sum_{i=1}^n Y_i-na-b\sum_{i=1}^n X_i=0,\\
&\sum_{i=1}^n X_iY_i-a\sum_{i=1}^n X_i-b\sum_{i=1}^n X_i^2=0.
\end{align*}
Zgornji dve enačbi imenujemo normalni enačbi. Z reševanjem teh enačb dobimo ocene najmanjših kvadratov za $a$ in $b$:
\begin{align*}
&\hat{a}=\overline{Y}-\hat{b}\overline{X},\\
&\hat{b}=\frac{\sum_{i=1}^n (X_i-\overline{X})(Y_i-\overline{Y})}{\sum_{i=1}^n (X_i-\overline{X})^2}=r_{X,Y}\frac{S_Y}{S_X}.
\end{align*}
Dobili smo enačbo $\hat{Y}=\hat{a}+\hat{b}X$ regresijske premice s najboljšim prileganjem (ang. \emph{best fit}) podatkom.
Označimo s $\hat{Y}_i=\hat{a}+\hat{b}X_i$ $i$-to predvideno vrednost (''fitano'' vrednost). Razliko 
\[
\hat{e}_i = Y_i-\hat{Y}_i=Y_i-\left(\hat{a}+\hat{b}X_i\right)
\]
imenujemo \emph{ostanki} (reziduali). Opazimo razliko med naključnimi napakami in ostanki: naključna napaka predstavlja razdaljo
točke od populacijske regresijsko premice, ostanek pa razdaljo točke od ocenjene regresijske premice. 



\section{Predpostavke linearnega regresijskega modela}
 \setlist{nolistsep}
\begin{enumerate}[noitemsep]
\item $Y$ je linearna funkcija $X$, 
\[
Y_i = a + b X_i + e_i, ~i = 1,2,\ldots, n,~\mbox{oziroma}, E(Y_i)=a+ b X_i,
\]
\item Napake $e_1,e_2,\ldots, e_n$ so neodvisne,
\item Napake $e_1,e_2,\ldots, e_n$ imajo konstantno varianco (homogenost variance),
\item Napake so normalno porazdeljene, $e\sim\mathcal{N}(0,\sigma)$.
\end{enumerate}

\vspace{0.5cm}
Varianco naključnih napak  $D(e)=\sigma^2$ ocenjujemo s
\[
S^2=\frac{1}{n-2}\sum_{i=1}^n \hat{e}_i^2
\]


\section{Intervala zaupanja za oceni naklona in odseka}

\subsection{Interval zaupanja za oceno naklona regresijske premice}
Ocena $\hat{b}$ je normalno porazdeljena, $\hat{b}\sim\mathcal{N}\left(b,\frac{\sigma}{\sqrt{n-1}S_X}\right)$.


Interval zaupanja za $b$ je enak
\[ I_{b}=\left[\hat{b}-c\frac{S}{\sqrt{n-1}S_X}, \hat{b}+c\frac{S}{\sqrt{n-1}S_X} \right],
\]
kjer je $c=t_{n-2;\frac{1+\beta}{2}}$ kvantil Studentove porazdelitve z $n-2$ prostostnimi stopnjami.

\subsection{Interval zaupanja za oceno odseka regresijske premice}
Ocena $\hat{a}$ je normalno porazdeljena, $\hat{a}\sim\mathcal{N}\left(a,\sigma\sqrt{\frac{1}{n}+\frac{\overline{X}^2}{(n-1)S_X^2}}\right)$.


Interval zaupanja za $a$ je enak
	\[ I_{a}=\left[\hat{a}-cS\sqrt{\frac{1}{n}+\frac{\overline{X}^2}{(n-1)S_X^2}}, \hat{\alpha}+cS\sqrt{\frac{1}{n}+\frac{\overline{X}^2}{(n-1)S_X^2}} \right],
	\]
kjer je $c=t_{n-2;\frac{1+\beta}{2}}$ kvantil Studentove porazdelitve z $n-2$ prostostnimi stopnjami.

\section{Testiranje linearne zveze med spremenljivkama}
Med spremenljivkama $Y$ in $X$ obstaja linearna zveza, če je
$Y=a+bX+e$ in $b\neq 0$. Če vemo, da je $b\neq 0$,
lahko predvidimo $Y$ z $\hat{Y}=\hat{a}+\hat{b}X$. Po drugi strani, če je
$b=0$, lahko predvidimo $Y$ z $\overline{Y}$. Drugače rečeno, če se linearni model 
dobro prilega podatkom, je predikcija $Y$ s pomočjo $\hat{Y}$ dobra, drugače 
se $Y$ boljše predvidi z vzorčnim povprečjem $\overline{Y}$.

Za testiranje linearnosti zveze med spremenljivkama $Y$ in $X$ bomo testirali ničelno domnevo
$H_0: b=0$ proti alternativni domnevi $H_1: b\neq 0$.

Ničelno domnevo lahko testiramo s pomočjo
\begin{enumerate}
\item t-testne statistike 
\item Fisherove testne statistike (analiza variance, opisana v Dodatku)
\end{enumerate}

\subsection{t-test za testiranje linearnega modela}
Testna statistika je
\[T=\frac{\hat{b}-b}{\frac{S}{\sqrt{n-1}S_X}},
\]
Če je ničelna domneva točna, ima testna statistika Studentovo porazdelitev z $n-2$ prostostnimi stopnjami. Če je
\[
|T|\geq c,
\]
kjer je $c=t_{n-2;1-\frac{\alpha}{2}}$ kvantil Studentove porazdelitve z $n-2$ prostostnimi stopnjami, zavrnemo ničelno domnevo.

\section{Interval predikcije za vrednost $Y$ pri določeni vrednosti $X=x_0$}
Zanima nas interval predikcije za bodočo (neopaženo)  vrednost $Y$ pri določeni vrednosti $X=x_0$.
\[I=\left[\hat{a}+\hat{b}x_0-cS\sqrt{1+\frac{1}{n}+\frac{(x_0-\overline{X})^2}{(n-1)S_X^2}},
~~\hat{a}+\hat{b} x_0+cS\sqrt{1+\frac{1}{n}+\frac{(x_0-\overline{X})^2}{(n-1)S_X^2}}\right]
\]
kjer je $c=t_{n-2;\frac{1+\beta}{2}}$ kvantil Studentove porazdelitve z $n-2$ prostostnimi stopnjami.


\section{Transformacija spremenljivk}
Če predpostavki normalnosti ali homogenosti variance naključnih napak nista izpolnjeni,
lahko poskusimo transformirati $Y$, oziroma vzeti $\log(Y)$, $Y^2$, $\sqrt{Y}$, $\frac{1}{Y}$ itn. za novo spremenljivko 
$Z$ in konstruirati novi linearni model odvisnosti $Z$ od $X$. Če odvisnost $Y$ od $X$ ni linearna,
lahko poskusimo s transformiranjem $X$, $Y$ ali oboje, nato konstruiramo novi linearni model
s transformiranima spremenljivkama. Če novi linearni model izpolnjuje predpostavke linearnega regresijskega modela
in se dobro prilega podatkom, lahko ga obdržimo. 




\section{Primer obdelave podatkov}
\subsection{Opis podatkov}
Zbrali smo vzorec mase in porabe goriva $32$ avtomobilov različnih znamk. Podatke smo zapisali v dokument, ki ima dva stolpca
\setlist{nolistsep}
\begin{itemize}[noitemsep]
\item $masa$ je numerična zvezna spremenljivka, ki predstavlja maso avtomobilov, merjeno v kilogramih. 
\item $kml$ je numerična zvezna spremenljivka, ki predstavlja porabo goriva avtomobila, merjeno
v številu prevoženih kilometrov na liter goriva (na dve decimalki).
\end{itemize}

Baza podatkov se imenuje \emph{avto.csv}. Najprej bomo prebrali podatke v R s pomočjo 
funkcije \emph{read.csv}, in zatem pogledali strukturo podatkov.

\begin{spacing}{1.5}
	
\texttt{avto$<$-read.csv("C:$\backslash\backslash$Users%
$\backslash\backslash$Kristina$\backslash\backslash$Documents$\backslash\backslash$avto.csv", header=TRUE)}
	
\texttt{str(avto)}
\end{spacing}

\textsf{
'data.frame':   32 obs. of  2 variables:\\
\$masa : int  1188 1304 1052 1458 1560 1569 1619 1447 1429 1560 ...\\
\$kml\quad: num  8.93 8.93 9.69 9.1 7.95 ...
}

\subsection{Opisna statistika} 
Zdaj bomo izračunali opisno statistiko -- povzetek s petimi števili (minimum, maksimum, prvi in tretji kvartil, mediano) in povprečno
vrednost mase in porabe goriva.  Za prikaz opisne statistike lahko uporabimo funkcijo \emph{summary}.

\begin{spacing}{1.5}
\texttt{summary(avto\$masa)}
\end{spacing}

\textsf{
$\begin{array}{cccccc}
\mbox{Min}.& \mbox{1st Qu.} & \mbox{Median}&\mbox{Mean} &\mbox{3rd Qu.} & \mbox{Max.} \cr
686&    1170&    1508&    1459&    1637&    2460
\end{array}$
}

Opažamo, da masa vzorca avtomobilov variira od 686 do 2460 kg, s povprečjem 1459 kg. 

\begin{spacing}{1.5}
\texttt{summary(avto\$kml)}
\end{spacing}

\textsf{
$\begin{array}{cccccc}
\mbox{Min}.& \mbox{1st Qu.} & \mbox{Median}&\mbox{Mean} &\mbox{3rd Qu.} & \mbox{Max.} \cr
 4.420&   6.558&   8.160&   8.541&   9.690&  14.410 
\end{array}$}

Opažamo, da poraba goriva vzorca avtomobilov variira od 4.42 do 14.41 prevoženih kilometrov na liter goriva, s povprečjem 8.541~km/l.
Razpon vrednosti mase in potrošnje goriva nam pomaga pri izbiri mej na oseh razsevnega diagrama.

Opisno statistiko lahko dobimo v eni tabeli z naslednjim ukazom 
\begin{spacing}{1.5}
\texttt{apply(avto, 2, summary)}
\end{spacing}

Funkcija \emph{apply} omogoča uporabo funkcije \emph{summary} na stolpca baze podatkov \emph{avto}. Izbira načina je odvisna od tega, kaj nam je bolj pregledno in enostavno.

\subsection{Razsevni diagram in koeficient korelacije}
Prikažimo dobljene podatke na razsevnem diagramu.
\begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\textwidth,height=0.5\textwidth]{slike/poraba.png}
\caption{Razsevni diagram mase in porabe goriva vzorca avtomobilov}
\end{center}
\end{figure}

\begin{spacing}{1.5}
\texttt{par(las=1, cex=1.4, mar=c(4,4,2,2))}

\texttt{plot(avto\$masa, avto\$kml, main="", xlim=c(500,2500), ylim=c(0,16), \\
~~~~xlab="Masa avtomobila (kg)", ylab="Poraba goriva (km/l)", axes=FALSE)}

\texttt{axis(1,pos=0,at=seq(500,2500,by=500),tcl=-0.2)}

\texttt{axis(2,pos=500,at=seq(0,15,by=5),tcl=-0.2)}

\texttt{arrows(x0=2500,y0=0,x1=2550,y1=0,length=0.1)}

\texttt{arrows(x0=500,y0=15,x1=500,y1=16,length=0.1)}
\end{spacing}

Funkcija \emph{par} omogoča postavljanje različnih nastavitev na grafih, preko velikega števila parame\-trov: 
\emph{las=1} omogoča horizontalne oznake na $y$-osi, različice parametra \emph{cex} povečanje simbolov, teksta, osi, itn., \emph{mar} 
pa prostor na robu, izražen s številom vrstic (privzeta vrednost je \emph{mar=c(5.1,4.1,4.1,2.1)} za spodnji, levi, zgornji in desni rob grafa).

Parameter \emph{main} služi za definiranje naziva grafa, \emph{xlab} in \emph{ylab} pa za nazive na $x$ in $y$ osi, 
ter \emph{xlim} in \emph{ylim} za postavljanje mej na oseh.

Osi privezetega R grafa se ne sekata v koordinatnem izhodišču (za podatke iz tega primera se osi ne sekata v $(500, 0)$, temveč v točki pod to). Z vrednostjo parametra
\emph{axes=FALSE} funkcije \emph{plot} smo navedli, da ne želimo privzete osi. Osi in oznake na njih
se lahko spremenijo s funkcijo \emph{axis}: \emph{axis(1)} riše $x$-os, \emph{axis(2)} pa $y$-os. Oznake na oseh se podajajo s parametrom \emph{at}, in presečišče osi
s parametrom \emph{pos}. Parameter \emph{tcl} opisuje dolžino črtic (ang. \emph{tick marks}). Dalje, puščice na oseh dodamo s pomočjo funkcije \emph{arrows},
ki se rišejo od točke $(x_0, y_0)$ do točke $(x_1, y_1)$ v dolžini opisani s parametrom \emph{length}.


Točke na razsevnem diagramu se nahajajo okoli namišljene premice, tako da linearni model zaenkrat izgleda kot primeren. Moč korelacije preverimo še z računanjem
Pearsonovega koeficienta korelacije.

\begin{spacing}{1.5}
\texttt{cor(avto\$masa,avto\$kml)}
\end{spacing}

\textsf{
[1] -0.8678461
}

Vrednost vzorčnega koeficienta korelacije je visoka ($r=-0.87$), kar govori o visoki linearni povezanosti mase avtomobilov in njihove porabe goriva. Dalje,
koeficient korelacije je negativen, kar pomeni, da avtomobili manjše mase prevozijo več kilometrov na liter goriva.

Na razsevnem diagramu mase avtomobilov in njihove porabe goriva opažamo 3 točke z visoko maso iznad $2000$ kg. Te točke so točke vzvoda, ki ne izgledajo
kot osamelci, ker sledijo linearnemu trendu ostalih točk. Identificirajmo te točke. Kasneje pri preverjanju predpostavk modela bomo videli, kakšen je njihov vpliv na model.

\begin{spacing}{1.5}
\texttt{avto\$masa[avto\$masa>2000]}
\end{spacing}

\textsf{
[1]~2381~ 2460~ 2424}


Avtomobili treh znamk imajo maso nad $2000$ kg.

\subsection{Formiranje linearnega regresijskega modela in preverjanje njegovih predpostavk}
Formirajmo linearni regresijski model s pomočjo funkcije \emph{lm}.


\begin{spacing}{1.5}
\texttt{(model$<$-lm(kml$\sim$masa,data=avto))}

\end{spacing}

\textsf{
Call:\\
lm(formula = kml$\sim$masa, data = avto)\\
Coefficients:\\
$\begin{array}{cc}
\mbox{(Intercept)}   & \mbox{masa}\\
15.85089 &    -0.00501  
\end{array}$}

Dobili smo ocenjeno regresijsko premico $\hat{y}=15.851-0.005x$, oziroma oceni naklona in odseka sta enaki $\hat{a}=15.851$ in $\hat{b}=-0.005$.


Predpostavke
linearnega regresijskega modela bomo preverili s pomočjo 4 grafov, ki se imenujejo tudi \emph{diagnostični grafi} (ali grafi za diagnostiko modela).
Če neke predpostavke niso izpolnjene, so lahko ocene neznanih parametrov, $p$-vrednost testa, intervali zaupanja 
in intervali predikcije netočni. 

S pomočjo parametra \emph{mfrow=c(2,2)} rišemo 4 grafe na eni sliki (2 vrstici in 2 stolpca). Grafe lahko dobimo z enim ukazom \texttt{plot(model, which=1:4)}, vse ostale spremembe se nanašajo na nazive na grafu v slovenščini. Na prvem in tretjem grafu je narisana funkcija glajenja (označena z rdečo barvo), ki se lahko odstrani s pomočjo parametra 
\emph{add.smooth=FALSE} (potem je ukaz za prvi graf \texttt{plot(model,which=1,caption="",add.smooth=FALSE, ann=F)}, za tretji graf pa \texttt{plot(model,which=3,caption="",add.smooth=FALSE,ann=F)}).

\vspace{0.5cm}
\begin{spacing}{1.5}
\texttt{par(mfrow=c(2,2),cex=1.1,mar=c(6,4.5,2,3))}

\texttt{plot(model,which=1,caption="",ann=F)}

\texttt{title(xlab=expression(italic(widehat(y)==widehat(a)+widehat(b)*x)),\\ 
ylab="Ostanki",main="Linearnost modela")}

\texttt{plot(model,which=2,caption="", ann=F)}

\texttt{title(xlab="Teoreti$\setminus$U10Dni kvantili", ylab= "St. ostanki", \\
main="Normalnost porazdelitve")}

\texttt{plot(model,which=3,caption="",ann=F)}

\texttt{title(xlab=expression(italic(widehat(y)==widehat(a)+widehat(b)*x)),\\
ylab=expression(sqrt(paste("|St. ostanki|"))), main="Homogenost variance")}

\texttt{plot(model,which=4,caption="", ann=F)}

\texttt{title(xlab="Meritev",ylab="Cookova razdalja", main="Vpliv to$\setminus$U10Dk na model")}
\end{spacing}


\begin{figure}[h]
\begin{center}
\includegraphics[width=\textwidth,height=0.8\textwidth]{slike/diag.png}
\caption{Diagnostični grafi za preverjanje predpostavk linearnega regresijskega modela}
\end{center}
\end{figure}



\subsubsection{Graf za preverjanje linearnosti modela}
Validnost linearnega regresijskega modela lahko preverimo tako, da narišemo
graf ostankov v odvisnosti od $x$ vrednosti ali od predvidenih vrednosti $\hat{y}=\hat{a}x+\hat{b}$ 
in poiščemo, ali obstaja neki vzorec. Če so točke raztresene na grafu in ne moremo zaznati neke oblike, je model validen. Če na grafu opazimo neki vzorec (npr. točke formirajo nelinearno funkcijo),
nam sama oblika vzorca daje informacijo o funkciji od $x$, ki manjka v modelu.

Za uporabljene podatke na grafu linearnosti modela ne opazimo vzorca. Točke so dokaj enakomerno razporejene nad in pod premico $Ostanki=0$. Na osnovi grafa bi lahko zaključili, da je linearni model validen.

\subsubsection{Graf normalnosti porazdelitve naključnih napak}
Normalnost porazdelitve naključnih napak preverjamo preko grafa porazdelitve standardiziranih ostankov. Ostanek se standardizira tako, da se deli z oceno njegovega standardnega odklona (njegovo matematično upanje je 0). Na $x$-osi $Q-Q$ grafa normalne porazdelitve se
predstavljajo teoretični kvantili, na $y$ - osi pa kvantili standardiziranih ostankov. Če dobljene točke na $Q-Q$ grafu tvorijo premico (z manjšimi odstopanji), zaključujemo, da je porazdelitev 
naključnih napak normalna.

Za podatke o masi in porabi goriva avtomobilov, lahko zaključimo, da so naključne napake normalno porazdeljene.

\subsubsection{Graf homogenosti variance}
Osnovna predpostavka linearnega regresijskega modela je, da imajo naključne napake konstantno varianco (homogenost variance). Učinkovit graf za
registriranje nekonstantne variance je graf korena standardiziranih ostankov v odvisnosti od $x$ ali od predvidenih vrednosti $\hat{y}=\hat{a}x+\hat{b}$.
Če variabilnost korena standardiziranih ostankov narašča ali pada s povečanjem vrednosti $\hat{y}$, je to znak, da varianca naključnih napak
ni konstantna. Pri naraščanju variance je graf pogosto oblike {\Huge $\lhd$}, in pri padanju variance oblike {\Huge $\rhd$}. Pri ocenjevanju lahko pomaga funkcija glajenja, 
v primeru konstantne variance se pričakuje horizontalna črta, okoli katere so točke enakomerno razporejene.

Za naš primer, na osnovi tretjega grafa na Sliki 1.4, točke na grafu sugerirajo,  da ni naraščanja ali padanja variance. Ničelna domneva konstantne variance se lahko formalno preveri s pomočjo testa konstantnosti variance (Breusch-Pagan test, funkcija \emph{ncvTest} v paketu \emph{car}). 
\begin{spacing}{1.5}
\texttt{ncvTest(model)}
\end{spacing}

\textsf{
Non-constant Variance Score Test \\
Variance formula: ~ fitted.values \\
Chisquare = 0.03700701, Df = 1, p = 0.84745
}

Na osnovi rezultata Breusch-Paganovega testa (testna statistika $\chi^2=0.037$, $df=1$, p-vrednost $p=0.847>0.05$), lahko sprejmemo ničelno domnevo, da 
je varianca naključnih napak konstantna.

\subsubsection{Graf vpliva posameznih točk na model}
Vpliv $i$-te točke na linearni regresijski model merimo s pomočjo Cookove razdalje
\[
D_i=\frac{\sum_{j=1}^n (\hat{Y}_{j(i)}-\hat{Y}_j)^2}{S^2},
\]
kjer je $\hat{Y}_j=\hat{a}+\hat{b}X_j$ $j$-ta predvidena vrednost, in $S$ ocena standardnega odklona naključnih napak. Z $\hat{Y}_{j(i)}$ smo
označili $j$-to predvideno vrednost linearnega modela, ki je narejen brez $i$ - te točke, na osnovi preostalih $n-1$ točk. 
Na ta način torej merimo razliko med modelom, ki vsebuje $i$-to točko in modelom, ki je ne vsebuje. Če $i$-ta točka ne vpliva močno na model,
bo $D_i$ majhna vrednost. 

Če je $D_i>\frac{4}{n-2}$, kjer je $n$ velikost vzorca, $i$-ta točka vpliva na linearni regresijski model. Dalje, če je tudi  $D_i\geq c$, kjer je $c=F_{2,n-2;0.5}$ mediana
Fisherove porazdelitve z 2 in $n-2$ prostostnima stopnjama, $i$-ta točka močno vpliva na regresijski model.

Kaj naj naredimo, če imamo podatkovne točke z velikim vplivom na linearni regresijski model?
\begin{enumerate}
\item  Vprašajmo se, ali so ti podatki neobičajni ali drugačni od ostalih podatkov? Če je odgovor pritrdilen, jih lahko
poskusimo odstraniti in konstruirati linearni model brez njih. 
\item Vprašajmo se, ali je linearni regresijski model validen za naše podatke? Mogoče podatkom boljše ustreza neki drugi model. Poskusimo konstruirati neki drugi model, ki vključuje še druge spremenljivke ali poskusimo s
transformiranjem $Y$ ali $X$. 
\end{enumerate}
\vspace{0.5cm}
Na grafu vpliva točk na linearni regresijski model so označene tri točke z najvišjo Cookovo razdaljo. Za naše podatke, to so 17., 18., in 20. podatkovne točka. 
Poglejmo ali vplivajo na model, oziroma če je njihova Cookova razdalja večja od $\frac{4}{n-2}=\frac{4}{30}=0.133$
\begin{spacing}{1.5}
\texttt{which(cooks.distance(model)>4/30)}
\end{spacing}
\textsf{
$\begin{array}{ccc}
17&18&20\\
17&18&20
\end{array}$}

Zdaj poglejmo po čem so te tri točke drugačne od ostalih. Lahko vizuelno pogledamo njihov položaj na razsevnem diagramu v primerjavi z drugimi točkami.
Kodi za razsevni diagram dodamo še dve vrstici, s katerima bomo dodali ocenjeno regresijsko premico in pobarvali te tri točke.

\begin{figure}[h]
\begin{center}
\includegraphics[width=0.7\textwidth,height=0.5\textwidth]{slike/tocke.png}
\caption{Razsevni diagram mase in porabe goriva avtomobilov z ocenjeno regresijsko premico in pobarvanimi točkami, ki vplivajo na model}
\end{center}
\end{figure}

\begin{spacing}{1.5}
\texttt{par(las=1, cex=1.4,mar=c(4,4,2,2))}
	
\texttt{plot(avto\$masa, avto\$kml, main="", xlim=c(500,2500), ylim=c(0,16),xlab=\\
"Masa avtomobila (kg)", ylab="Poraba goriva (km/l)",axes=FALSE)}
	
\texttt{axis(1,pos=0,at=seq(500,2500,by=500),tcl=-0.2)}
	
\texttt{axis(2,pos=500,at=seq(0,15,by=5),tcl=-0.2)}
	
\texttt{arrows(x0=2500,y0=0,x1=2550,y1=0,length=0.1)}
	
\texttt{arrows(x0=500,y0=15,x1=500,y1=16,length=0.1)}

\texttt{abline(model,lwd=2)}

\texttt{points(avto\$masa[c(17,18,20)],avto\$kml[c(17,18,20)],labels=\\
avto\$model[c(17,18,20)],col="blue",pch=19)}
\end{spacing}



Na razsevnem diagramu opazimo, da so vse tri točke najbolj oddaljene od ocenjene regresijske premice (oziroma jim ustrezajo največji ostanki). Lahko preverimo še, ali je njihov vpliv velik, oziroma ali je njihova Cookova razdalja večja ali enaka od mediane Fisherove porazdelitve s $2$ in $30$ prostostnimi stopnjami.

\begin{spacing}{1.5}
\texttt{any(cooks.distance(model)[c(17,18,20)]$>$=qf(0.5,2,30))}
\end{spacing}

\textsf{
[1] FALSE}

Nobena od teh točk nima velikega vpliva na linearni regresijski model, zato jih ni potrebno odstraniti.



\subsection{Testiranje linearnosti modela in koeficient determinacije}
Poročilo o modelu, skupaj z rezultati t-testa za testiranje linearnosti modela in koeficientom determinacije, dobimo
s pomočjo funkcije \emph{summary}.
\begin{spacing}{1.5}
\texttt{summary(model)}
\end{spacing}

\textsf{
Call:\\
lm(formula = kml $\sim$ masa, data = avto)}
\vspace{0.2cm}

\textsf{
Residuals:\\
$\begin{array}{ccccc}
\mbox{Min}& \mbox{1Q}&\mbox{Median}& \mbox{3Q}&\mbox{Max}\\
-1.9269& -1.0045& -0.0551&  0.6003&  2.9188
\end{array}$}
\vspace{0.4cm}

\textsf{
Coefficients:\\
$\begin{array}{lcccc}
&\mbox{Estimate}&\mbox{Std. Error} & \mbox{t value}&Pr(>|t|)   \\ 
\mbox{(Intercept)}& 15.8508866&  0.7975736&  19.874&  < 2e-16 ***\\
\mbox{masa}&        -0.0050097&  0.0005236&  -9.567& 1.27e-10 ***
\end{array}$}

\textsf{
---\\
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1}
\vspace{0.3cm}

\textsf{Residual standard error: 1.294 on 30 degrees of freedom\\
Multiple R-squared:  0.7532,    Adjusted R-squared:  0.7449 \\
F-statistic: 91.53 on 1 and 30 DF,  p-value: 1.268e-10}

Nas zanima samo en del teh rezultatov. Testna statistika za testiranje linearnosti modela je $T=-9.567$, s $df=30$ prostostnimi stopnjami in s p-vrednostjo $p=1.27e-10$, ki je manjša od dane stopnje značilnosti
$0.05$. Na osnovi rezultatov t-testa zavrnemo ničelno domnevo $H_0: b=0$, za dano stopnjo značilnosti in dobljeni vzorec. Drugače rečeno, s formalnim statističnim testiranjem
smo potrdili, da linearni model ustreza podatkom.

Standardni odklon naključnih napak je ocenjen s $S=1.294$. 

Koeficient determinacije je enak $R^2=0.753$, kar je kvadrat vzorčnega koeficienta korelacije. To pomeni, 
da $75\%$ variabilnosti porabe goriva pojasnjuje linearni regresijski model.

\subsection{Intervala zaupanja za naklon in odsek regresijske premice}
Izračunajmo $95\%$ interval zaupanja za neznani naklon in odsek regresijske premice s pomočjo funkcije \emph{confint}.
\begin{spacing}{1.5}
\texttt{round(confint(model),3)}
\end{spacing}

\textsf{
$\begin{array}{lcc}
&2.5\%& 97.5\%\\
\mbox{(Intercept)}& 14.222& 17.480\\
\mbox{masa}&       -0.006& -0.004
\end{array}$}

Interval zaupanja za odsek je enak $I_a=[14.222,~ 17.480]$ in interval zaupanja za naklon $I_b=[-0.006,~ -0.004]$.

\subsection{Interval predikcije za vrednost $Y$ pri izbrani vrednosti $X$}
Pri predvidevanju vrednosti porabe goriva, nas zanima bodoča (neopažena) vrednost spremenljivke $Y$ pri izbrani vrednosti
spremenljivke $X=x_0$. Ne zanima nas samo predvidena vrednost $\hat{y}=15.851-0.005x_0$ avtomobilov mase $x_0=1000, 1500, 2000$ kg, 
vendar želimo vedeti, z gotovostjo $95\%$, v kakšnih mejah se nahaja 
poraba goriva vseh avtomobilov teh mas. Interval predikcije najdemo s pomočjo funkcije \emph{predict}.

\begin{spacing}{1.5}
\texttt{xmasa = data.frame(masa=c(1000,1500,2000))}\\
\texttt{predict(model, xmasa, interval="predict")}
\end{spacing}

\textsf{
$\begin{array}{lccc}
&\mbox{fit}& \mbox{lwr}&\mbox{upr}\\
1&10.841177& 8.113546& 13.568808\\
2&  8.336323& 5.652908& 11.019737\\
3&  5.831468& 3.086793 & 8.576144
\end{array}$}

\vspace{0.2cm}
Predvidena vrednost porabe goriva za avtomobil mase (na celi populaciji avtomobilov)
\begin{enumerate}
\item 1000~kg je 10.84~km/l, s $95\%$ intervalom predikcije porabe goriva $[8.11, ~13.57]$,
\item 1500~kg je 8.34~km/l, s $95\%$ intervalom predikcije porabe goriva $[5.65, ~10.02]$,
\item 2000~kg je 5.83~km/l, s $95\%$ intervalom predikcije porabe goriva $[3.09, ~8.58]$
\end{enumerate}

Predikcijo vrednosti slučajne spremenljivke $Y$ je smiselno narediti v okviru razpona $x$ vrednosti v vzorcu (oziroma: interpolacija je smiselna, ekstrapolacija pa ne).


\chapter{Dodatek}
\section{Interval zaupanja}
Interval zaupanja nam daje interval okoli vrednosti neznanega parametra porazdelitve, z izbrano stopnjo zaupanja.

\subsection{Interval zaupanja za neznano povprečje populacije}
Za simetrične porazdelitve pogosto vzamemo povprečno vrednost vzorca kot dobrega 
predstavnika centralne vrednosti. Vemo, da je ta povprečna vrednost ocena
neznane povprečne vrednosti cele populacije. Tudi vemo, da smo v vzorcu zaobjeli samo del populacije 
- čim večji del populacije, oziroma čim večja velikost vzorca, tem boljša ocena neznanega povprečja populacije
je vzorčno povprečje. Dalje, vrednosti vzorca varirajo okoli povprečne vrednosti, 
zato je potrebno vzeti v zakup tudi disperzijo. 

S formiranjem intervala zaupanja želimo z veliko gotovostjo določiti, v katerih mejah se giba
neznana povprečna vrednost populacije. Običajno se vzame stopnja zaupanja $90\%$, $95\%$ ali $99\%$,
oziroma rečemo, da smo npr. $95\%$ prepričani, da se neznano povprečje populacije nahaja v tem intervalu.
To pomeni, da če bi npr. vzeli 100 vzorcev in izračunali interval zaupanja za vsak vzorec, bi povprečje populacije 
pripadalo najmanj 95 intervalom.

\subsection{Interval zaupanja za povprečno vrednost $Y$ pri določeni vrednosti $X=x^*$}
Zanima nas povprečna vrednost slučajne spremenljivke $Y$ pri določeni vrednosti $X=x^*$. Interval zaupanja za povprečno vrednost $Y$ pri $X=x^*$ 
je enak
\[I=\left[\hat{a}+\hat{b}x^*-cS\sqrt{\frac{1}{n}+\frac{(x_0-\overline{X})^2}{(n-1)S_X^2}},~~
\hat{a}+\hat{b} x^*+cS\sqrt{\frac{1}{n}+\frac{(x_0-\overline{X})^2}{(n-1)S_X^2}}\right]
\]
kjer je $c=t_{n-2;\frac{1+\beta}{2}}$ kvantil Studentove porazdelitve z $n-2$ prostostnimi stopnjami.




\section{Statistični test}
Želimo testirati \emph{ničelno domnevo} $H_0$ proti \emph{alternativni domnevi} $H_1$. 

Vsak test ima definirano \emph{testno statistiko}, neko funkcijo 
vzorca, ki nam omogoča testiranje ničelne domneve.

Dalje, pri pogoju, da je $H_0$ točna, ima testna statistika določeno porazdelitev.
To pomeni, da če bi vzeli npr. 100 vzorcev in za vsak vzorec
izračunali testno statistiko $T_1,T_2,\ldots T_{100}$, bi dobili določeno porazdelitev testne statistike.

Pri parametričnih testih se predpostavlja, da imajo podatki neko porazdelitev (pogosto se predpostavlja,
da je porazdelitev podatkov normalna). Neparametrični testi NIMAJO predpostavke o porazdelitvi podatkov.

Preden testiramo neko domnevo, izberemo stopnjo značilnosti $\alpha$ (verjetnost napake 1. vrste ali lažno 
pozitivnega rezultata). Najbolj pogosto se vzame $\alpha=0.05$. Ne smemo pozabiti, da je naključnost sestavni del 
statističnega testiranja, ker delamo s slučajni vzorci in se sprijaznimo 
s tem, da obstaja verjetnost lažno pozitivnega rezultata $\alpha$ in, da to NE predstavlja napako raziskovalca. 
Raziskovalec je dolžen nabrati kakovostne podatke, ki verno predstavljajo proučevano populacijo.

Za izbrano stopnjo značilnosti, alternativno domnevo in na osnovi porazdelitve testne statistike, najdemo kritično območje za ničelno domnevo.
Če vrednost testne statistike na vzorcu pade v kritično območje, ničelno domnevo zavrnemo in sprejmemo alternativno domnevo.
Na drugi način, lahko izračunamo $p$-vrednost, oziroma verjetnost, ki ustreza vrednosti testne statistike. Če je $p$-vrednost 
manjša od izbrane stopnje značilnosti, zavrnemo ničelno domnevo.

Statistični test je zamišljen tako, da predstavlja objektiven, ponovljiv in zanesljiv način testiranja naše domneve.
Da bi bili prepričani, da test dobro dela, je najprej nujno preveriti predpostavke testa, ali pogoje njegove uporabe.

\begin{tcolorbox}
	
Deli statističnega testa
\setlist{nolistsep}
\begin{enumerate}
\item Ničelna in alternativna domneva
\item Stopnja značilnosti
\item Predpostavke testa (pogoji, pri katerih se lahko uporablja)
\item Testna statistika in njena porazdelitev, za točno ničelno domnevo
\item Kritično območje
\end{enumerate}
\end{tcolorbox}


\subsection{Analiza variance za testiranje linearnega modela}
Pristop analize variance je posebej koristen pri multipli regresiji, oziroma ko imamo
več kot en prediktor. Definirajmo tri vsote kvadratov odstopanj.

Celotna (totalna) vsota kvadratov odstopanj vrednosti $Y_i$ od vzorčnega povprečja $\overline{Y}$ je
\[
S_T^2=\sum_{i=1}^n (Y_i-\overline{Y})^2.
\]

Rezidualna vsota kvadratov odstopanj vrednosti $Y_i$ od predvidene vrednosti $\hat{Y}_i$ je
\[
S_R^2=\sum_{i=1}^n (Y_i-\hat{Y}_i)^2.
\]

Regresijska vsota kvadratov odstopanj vrednosti $\hat{Y}_i$ od vzorčnega povprečja $\overline{Y}$ (vsota kvadratov regresijskega modela)
\[
S_M^2=\sum_{i=1}^n (\hat{Y}_i-\overline{Y})^2.
\]

Dalje, enostavno se pokaže, da velja
$$\begin{array}{ccccc}
S^2_T&=&S^2_M&+&S^2_R\\
\mbox{Totalna variabilnost vzorca}&=&\mbox{Variabilnost pojasnjena z modelom}&+&\mbox{Nepojasnjena variabilnost}
\end{array}$$
Če velja $Y=a+bX+e$ in $b\neq 0$, je potrebno, da je vsota $S^2_R$ dovolj majhna in $S^2_M$ dovolj blizu $S^2_T$, Koliko je to "dovolj majhno"
in "dovolj blizu"?

Za testiranje 
\[
H_0: b=0~  \mbox{proti}~ H_1: b\neq 0
\]
uporabljamo testno statistiko

\[
F=\frac{\frac{S^2_M}{1}}{\frac{S^2_R}{n-2}}
\]

Kako smo prišli do te testne statistike?
Pri točni ničelni domnevi,
$\frac{S^2_M}{\sigma^2}$ ima $\chi^2$ porazdelitev z 1 prostostno stopnjo, $\frac{S^2_M}{\sigma^2}\sim\chi^2_{1}$ in $\frac{S^2_R}{\sigma^2}$ ima $\chi^2$ porazdelitev z 
$n-2$ prostostnih stopenj, $\frac{S^2_R}{\sigma^2}\sim\chi^2_{n-2}$. Dalje, testna statistika $F$ predstavlja ulomek dveh 
$\chi^2$ porazdeljenih spremenljivk, deljenih s ustreznimi prostostnimi stopnjami
\[
F=\frac{\frac{S^2_M}{1\cdot\sigma^2}}{\frac{S^2_R}{(n-2)\sigma^2}}=\frac{\frac{S^2_M}{1}}{\frac{S^2_R}{n-2}}.
\]
Torej, statistika $F$ ima Fisherovo porazdelitev z $1$ in $n-2$ prostostnimi stopnjami, $F\sim\mathcal{F}_{1,n-2}$.
Ničelno domnevo zavrnemo za velike vrednosti testne statistike, oziroma za $F\geq c$, kjer je $c=F_{1,n-2; 1-\alpha}$.

Običajno dobimo kot rezultat testiranje naslednjo tabelo.
\begin{center}
\renewcommand\arraystretch{1.3}
\begin{tabular}{ccccc}\hline
\mbox{Vir variabilnosti}&\mbox{Prostostne stopnje}&\mbox{Vsota kvadratov}&\mbox{Povprečen kvadrat}&F\\\hline
\mbox{Regresijski model}&1&$S^2_M$&$\frac{S^2_M}{1}$&\multirow{2}*{$F=\frac{\frac{S^2_M}{1}}{\frac{S^2_R}{n-2}}$}\\	
\mbox{Reziduali}&$n-2$&$S^2_R$&$\frac{S^2_R}{n-2}$& \\	
\mbox{Total}&$n-1$&$S^2_T$&& \\\hline
\end{tabular}
\end{center}

V primeru enostavne linearne regresije obstaja zveza med testnima statistikama $T$ in $F$, ki se uporabljata za testiranje
linearnosti modela. Velja $F=T^2$.


\vfill
\begin{figure}[h!]
	\includegraphics[width=0.2\textwidth]{slike/by-nc-ndeu.png}
	\hspace{1em}Kristina Veljković, 2020
\end{figure}


To delo je licencirano pod licenco Creative Commons Priznanje
avtorstva-Nekomercialno-Brez predelav 4.0 Mednarodna. Kopija licence
se nahaja na sledeči povezavi:
\small{
\url{http://creativecommons.org/licenses/by-nc-nd/4.0/}}
\end{document}




